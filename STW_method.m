%% Check mainshocks foreshocks and aftershocks according to
% Agnew and Jones, 1991; Marzocchi and Zhuang, 2011, Seif et al., 2019

function [idx_all_main_for_each_class,...
    all_previous_event_for_each_class,idx_main_for_each_class,...
    all_previous_event_larger_than_0,n_mainshocks_rc,...
    n_foreshocks_rc,n_foreshocks_real] = STW_method(Lat,Long,...
    Magn,time_rel,m_M,m_L,m_main,spacing,L,T_for,y,y2)

% INPUT variables:  
% m_M = mainshock magnitude class %we set [4:0.5:6] note the last class has
%       not a right edge (all mainshocks M>6 are part of the last class). 
% m_L = foreshock magnitude class %we set [2.5:0.5:4]
% m_main = minimum magnitude of mainshocks used for analyses, %we set = 4
% To find events M > 5, set:
% L = distance to find foreshocks M > 5 [km] %we set = 100
% T_for = time to find foreshocks M > 5 [days] %we set = 180
% set time and space before a mainshock to find events M > m_main:
% y = time before a mainshock to find larger foreshocks [days] %we set = 3
% y2 = distance from mainshock to find larger foreshocks [km] %we set = 10

% OUTPUT variables: 
% --- Mainshocks
%index of all mainshocks for each class
idx_all_main_for_each_class = cell(length(m_M),length(m_L));
%index of mainshocks with foreshocks > 0 for each class
idx_main_for_each_class = cell(length(m_M),length(m_L));

% --- Foreshocks
%number of foreshocks (also zero) of each mainshocks for each class
all_previous_event_for_each_class = cell(length(m_M),length(m_L));
%number of foreshocks (> zero) of each mainshock for each class
all_previous_event_larger_than_0 = cell(length(m_M),length(m_L));

%normalized number of foreshocks for each class
n_foreshocks_rc = NaN(length(m_M),length(m_L));
%number of mainshocks with foreshocks >0 for each class
n_mainshocks_rc = NaN(length(m_M),length(m_L));
%number of foreshock for each class (without normalization)
n_foreshocks_real = NaN(length(m_M),length(m_L)); 

%add subfolder "extra-functions" as the main path
addpath('/extra-functions/');

%% start

% Latitude-to-km conversion factor (for coordinate differences)
Lat_km = 110.574;
n=length(Lat);
idx_events = 1:n;  % selection helper
cc = 0;

% -- EXTRACT relative times

for mag_cl =  m_M % loop over magnitude mainshock class

    cc = cc +1; %as support to store mainshock classes
    c = 0; %as support to store data for each for/aftershock class

    for L_class = m_L % loop over for/aftershocks magnitude threshold


        % Obtain indices for events within a magnitude threshold
        idx_ref = idx_events(Magn >= mag_cl & Magn < mag_cl + spacing);

        %last mainshocks class
        if mag_cl == m_M(end)
            idx_ref = idx_events(Magn >= mag_cl & Magn < mag_cl + m_main);
        end

        idx_all_main = [];
        idx_main = [];
        n_for_for_each_main = [];
        n_for_larger_0 = [];


        % Count the number of ignored mainshocks due to a larger ...
        n_ignored1 = 0;  % foreshocks > mainshock
        n_ignored2 = 0;  % aftershock > mainshock

        for idx_m = idx_ref  % loop over each reference event (by index)

            % Obtain relative times
            % (positive values are potential aftershocks, negative potential foreshocks)
            t_rel = time_rel - time_rel(idx_m);  %difference between mainshocks and all events

            % 1. Filter by time (- 180 days)
            t_sel = t_rel >= -T_for & t_rel < 0;   %events 180 days BEFORE mainshock
            t_sel_2 = t_rel > 0 & t_rel <= y; %events 3 days AFTER mainshock

            idxsel_t = idx_events(t_sel);  %index events in 180 days BEFORE mainshock
            idxsel_t_2 = idx_events(t_sel_2); %index events in 3 days AFTER mainshock

            % 2. Filter by space
            Lon_km = 111.320 * cos(Lat(idx_m) * pi / 180);  % current Longitude-to-km factor

            %compute distance between mainshock and each possible
            %foreshocks
            %distance mainshock and all events 180 days before
            dists = sqrt(((Long(t_sel) - Long(idx_m)) * Lon_km).^2 + ...
                ((Lat(t_sel) - Lat(idx_m)) * Lat_km).^2);
            %distance between mainshocks and each aftershock
            dists_2 = sqrt(((Long(t_sel_2) - Long(idx_m)) * Lon_km).^2 + ...
                ((Lat(t_sel_2) - Lat(idx_m)) * Lat_km).^2);


            % 3. Account for mainshock selection criteria (check if need to skip reference event)
            %we take all the space-diff in a radious of 100 km and 180 days before
            idxsel_Lt = idxsel_t(dists <= L);

            %we take all the space-diff in a radious of 10 km and 180 days before
            idxsel_Lt2 = idxsel_t(dists <= y2);

            %take the events in 10 km and 3 days before
            idxsel_Lt3 = idxsel_Lt2(-y <= t_rel(idxsel_Lt2) & t_rel(idxsel_Lt2) < 0);

            %take all the space-diff in a radious of 10 km and 3 days after
            idxsel_aftershock = idxsel_t_2(dists_2 <= y2);

            if sum(Magn(idxsel_Lt) > 5)       %check a foreshocks M > 5
                n_ignored1 = n_ignored1 + 1;
                continue
            elseif sum(Magn(idxsel_Lt3) > Magn(idx_m))  %foreshock with M > mainshocks
                n_ignored2 = n_ignored2 + 1;
                continue
            end

            %% 4. Aftershocks of each selected mainshock
            %if we do not have foreshock magnitude larger than
            %mainshocks, we collect the relative time of their
            %for/aftershocks
            aftersh_mainsh = [idx_m,idxsel_aftershock];

            %% Check if mainshocks have foreshocks
            % Number of foreshocks of the mainshock 
            % using different forshock magnitude threshold

            M_sel =  Magn(idxsel_Lt3) >= L_class;     %select for forshock magnitude threshold
            idxsel_mL = [idxsel_Lt3(M_sel), idx_m];   %take the foreshock index + index of mainshock
            idxsel_mL = sort(unique(idxsel_mL));     %this is when fors and mainshock are = m_main
            idxsel_mL2 = (idxsel_Lt3(M_sel))';   %take index of only foreshocks of that class
            %as support to check foreshocks in the first class
            M_sel_2 = nnz(Magn(idxsel_Lt3) >= m_L(1));

            %% Index of the mainshocks and count the foreshocks
            %take only real mainshock, excluding single events

            %if mainshock has aftershocks
            if length(aftersh_mainsh) > 1
                %---> no foreshocks
                if length(idxsel_mL) < 2
                    n_for_for_each_main = [n_for_for_each_main;0];
                    idx_all_main = [idx_all_main;idx_m];
                    %---> yes foreshocks
                elseif length(idxsel_mL) > 1
                    n_for_for_each_main = [n_for_for_each_main;length(idxsel_mL2)];
                    idx_all_main = [idx_all_main;idx_m];
                    idx_main = [idx_main; idx_m];
                    n_for_larger_0 = [n_for_larger_0;length(idxsel_mL2)];
                end

                %if does not have aftershocks
            elseif length(aftersh_mainsh) < 2
                %but has foreshocks in that class
                if length(idxsel_mL) > 1
                    idx_all_main = [idx_all_main; idx_m];
                    n_for_for_each_main = [n_for_for_each_main;length(idxsel_mL2)];
                    idx_main = [idx_main; idx_m];
                    n_for_larger_0 = [n_for_larger_0;length(idxsel_mL2)];
                    %if does not have foreshocks in that class
                    %but has foreshocks >= 2.5, so in the first class:
                elseif M_sel_2 > 0
                    n_for_for_each_main = [n_for_for_each_main;0];%<-- put 0 foreshocks in that class
                    idx_all_main = [idx_all_main; idx_m];
                end
            end

        end
        % Save infos for this magnitude class
        c = c+1;

        %all mainshocks for each class (with and without foreshocks)
        idx_all_main_for_each_class(cc,c) = {idx_all_main};
        % mainshocks that have foreshocks (>0)
        idx_main_for_each_class(cc,c) = {idx_main};

        %count the number of foreshock (also zero foreshocks)
        all_previous_event_for_each_class(cc,c) = {n_for_for_each_main};
        %count the number of foreshocks (>0)
        all_previous_event_larger_than_0(cc,c) = {n_for_larger_0};

        % Extract all relevant fore/aftershocks & append
        n_mainsh = length(idx_main);
        n_foresh = sum(n_for_larger_0)./n_mainsh;

        n_mainshocks_rc(cc,c) = (n_mainsh);        %N.mainshock for each magnitude class
        n_foreshocks_rc(cc,c) = (n_foresh);        %N.foreshocks for each magnitude class
        n_foreshocks_real(cc,c) = sum(n_for_larger_0);
    end
end

end