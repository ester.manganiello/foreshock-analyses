%% Compute the normalized number of foreshocks for each class
% We consider only sequences that have foreshocks (>0)
% We normalize the number of foreshocks for each class by the total
% number of sequences of that class
% we make a CDF of the NORMALIZED number of forshocks of synthetic catalogs
% for each class

function [idx_main_with_anomalous_foresh, idx_main_for_each_class,...
    anomalous_sequences,percent_selected,m_M,m_L] = ...
    TEST1(n_foreshocks_rc,all_previous_event_larger_than_0,...
    n_foreshocks_synt_cat,percent_selected,p_value_selected,...
    idx_main_for_each_class,n_mainshocks_rc,m_M,m_L)

% ---> INPUT variables are obtained after running NN_method.m or STW_method.m
% all input variables are explained in that file.m
% To set:
% p_value_selected = p_value % we set 0.01 or 0.05;
% percent_selected = percentile % we set 99th or 95th percentile

% OUTPUT variables
%idx of anomalous mainshocks
idx_main_with_anomalous_foresh = cell(length(m_M),length(m_L));
%number of anomalous sequences
anomalous_sequences = [];

%add subfolder "extra-functions" as the main path
addpath('/extra-functions/');

%% Start analysis

% Histogram for each mainshock and foreshocks class
figure('Position',[0 0 1000 1000],'Color',[1 1 1]);
tlo = tiledlayout(length(m_M),length(m_L),'Padding', 'none',...
    'TileSpacing', 'compact');

c = 0; %number of cicles
range_on_figures = (1:length(m_L):length(m_L)*length(m_M));
percentile_n_for_99 = NaN(length(m_M),length(m_L));
p_value_REAL = NaN(length(m_M),length(m_L));
h_test_REAL = NaN(length(m_M),length(m_L));

for rows = 1:length(m_M)
    for columns = 1:length(m_L)

        c = c+1;
        ax_lgd = nexttile(c);
        ax(c) = nexttile(c);

        hold on
        idx_main_anom = [];
        ppp=0;

        %-------- 1. Plot the CDF
        %histrogram for each magnitude class
        ecdf(vertcat(n_foreshocks_synt_cat{rows,columns,:}));%--> all normalized n_foreshocks for a specific class
        h_ecdf = get(gca,'children');
        set(h_ecdf(1),'color','k','Linestyle','--');
        hold on

        %-------- 2. Compute 99 percentile of the CDF
        percentile_n_for_99(rows,columns) =(prctile...
            (vertcat(n_foreshocks_synt_cat{rows,columns,:}),percent_selected));

        %-------- 3. Compute p_value and h_value
        appogg_synth = vertcat(n_foreshocks_synt_cat{rows,columns,:});
        %exclude nan values for mean
        %REMEMBER!! --> nan values are due to cases where mainshocks with 
        % foreshocks do not exist in a specific class
        appogg_synth = appogg_synth(appogg_synth>0);
        perc_of_n_forsh_rc = mean(appogg_synth...
            <=n_foreshocks_rc(rows,columns))*100;%--> alternative way to compute the percentile
        p_value_REAL(rows,columns) = ((100 - perc_of_n_forsh_rc)/100);%--> p_value 

        % H_value
        if p_value_REAL(rows,columns)  >= p_value_selected
            h_test_REAL(rows,columns)  = 0; %do not reject the null hypot
        else
            h_test_REAL(rows,columns)  = 1; %reject the null hypothesis

            %--------------- 4. Anomalous sequences
            % Keep as anomalous the sequences with a number of
            % foreshocks above the xx-percentile (rounded up)
            idx_main_anom = [];
            
            %loop on each mainshock
            for z_main = find(cell2mat(idx_main_for_each_class(rows,columns)))'
                %compare the number of foreshocks for each sequence
                %with the xx-percentile
                if all_previous_event_larger_than_0{rows,columns}(z_main)...
                        >= ceil(percentile_n_for_99(rows,columns)) %-->round up the percentile
                    
                    %if the number of foreshocks is larger/equal than the
                    %xx-percentile (rounded up), take track of index of the
                    %mainshocks
                    idx_main_anom = [idx_main_anom;...
                        idx_main_for_each_class{rows,columns}(z_main)];%<--take the index of the mainshocks
                end
            end
            %anomalous sequences for each class
            idx_main_with_anomalous_foresh{rows,columns} = (idx_main_anom);
            %number of anomalous sequences for each class (for annotation plot)
            numel_of_anom_sequ = length(idx_main_anom);
        end
        %count the total number of anomalous sequences from this test
        anomalous_sequences = unique(vertcat(idx_main_with_anomalous_foresh{:,:}));

        %--------- 5. Plot xx-percentile on the plot 
        xline(percentile_n_for_99(rows,columns),'--',  'LineWidth',2)

        %just to show black line in the legend
        hold on
        plot(nan,nan,'k-','LineWidth',1.5)
        hold on

        %--------- 6. Plot for Real catalog
        n_foreshocks_rc(isnan(n_foreshocks_rc(:))) = 0;
        color_for_anom = hex2rgb('#ec3c0d'); % REF. for function:Chad Greene (2022). 
        % rgb2hex and hex2rgb (https://www.mathworks.com/matlabcentral/fileexchange/46289-rgb2hex-and-hex2rgb),
        % MATLAB Central File Exchange. Retrieved March 12, 2022. 

        if isempty(idx_main_with_anomalous_foresh{rows,columns})
            hold on
            xline(n_foreshocks_rc(rows,columns),'Color','k','LineStyle','-','LineWidth',1.5)
        else
            hold on
            xline(n_foreshocks_rc(rows,columns),'Color',color_for_anom,'LineStyle','-','LineWidth',1.5)
        end


        %Label
        yticks([0 0.5 1])
        bb = n_foreshocks_rc(rows,columns)+...
            percentile_n_for_99(rows,columns)+...
            (30*percentile_n_for_99(rows,columns)/100);
        xlim([0 bb])
        xlabel('')
        ylabel('')

        %Legend (add legend only at the first cicle)
        if rows == 1
            if columns == 1
                %xlabel
                xlabel(tlo,'Normalized foreshocks, N̂','FontName','Arial',...
                    'FontWeight','bold','FontSize',12)
                %legend
                lg  = legend(ax_lgd,'CDF of N̂^E^T^A^S_F',join(sprintf...
                    ('%g^t^h percentile of CDF',percent_selected)),...
                    'N̂_F^r^e^a^l','N̂_F^r^e^a^l (Anomalies)','Orientation','Vertical',...
                    'NumColumns',4,'FontSize',11,'FontName','Arial');
                lg.Layout.Tile = 'North'; % <-- Legend placement with tiled layout
            end
        end

        %Condition: title above the first 4 subplot
        if c>=1 && c<=length (m_L)
            Title_names = join(["m_F ≥ ", m_L(columns)]);
            title(Title_names,'FontWeight','bold','FontSize',12)
        end

        %Condition: y axis only to the left subplot
        if c == range_on_figures(end)
            y_label_names_last = join(sprintf('m_M ≥ %.1f',m_M(end)));
            ylabel(y_label_names_last,'FontWeight','bold','FontSize',12)
        elseif ismember(c,range_on_figures)
            y_label_names = join(sprintf('%.1f ≤ m_M < %.1f',m_M(rows),m_M(rows+1)));
            ylabel(y_label_names,'FontWeight','bold','FontSize',12);
        end

        %----- 5. Annotation

        % Annotation for number of Anomalous Sequences for each class
        if isempty(idx_main_with_anomalous_foresh{rows,columns})
            n_anom_seq = sprintf('N_A_F_S = 0');
        else
            n_anom_seq = sprintf('N_A_F_S = %.f',numel_of_anom_sequ);
        end

        % Annotation for P_VALUE for each class
        if p_value_REAL  (rows,columns) > p_value_selected
            ppp = sprintf('p = %.3f ',round(p_value_REAL(rows,columns),...
                3,'significant'));
        elseif p_value_REAL (rows,columns) <= p_value_selected
            ppp = sprintf('p < %g ', p_value_selected);
        end

        % Annotation for total number of mainshocks for each class
        n_M = sprintf('N_M = %.f', n_mainshocks_rc(rows,columns));

        hold on
        % Show all annotations 
        if p_value_REAL  (rows,columns) > p_value_selected
            
            str = {n_anom_seq,'', '','', n_M};
            str_2 =  {ppp};
            e(c) = annotation('textbox','String',str,...
                'FitBoxToText','on','Color','k',...
                'FontSize',10,'Position',ax(c).Position,...
                'HorizontalAlignment','right','Vert','middle',...
                'EdgeColor','none','Margin',2);
            hold on
            e(c) = annotation('textbox','String',str_2,...
                'FitBoxToText','on','Color','k',...
                'FontSize',10,'Position',ax(c).Position,...
                'HorizontalAlignment','right','Vert','middle',...
                'EdgeColor','none','Margin',2);

        elseif p_value_REAL (rows,columns) <= p_value_selected
            str = {n_anom_seq,'', '','', n_M};
            str_2 =  {ppp};
            e(c) = annotation('textbox','String',str,...
                'FitBoxToText','on','Color','k',...
                'FontSize',10,'Position',ax(c).Position,...
                'HorizontalAlignment','right','Vert','middle',...
                'EdgeColor','none','Margin',2);
            hold on
            e(c) = annotation('textbox','String',str_2,...
                'FitBoxToText','on','Color','k',...
                'FontSize',10,'Position',ax(c).Position,...
                'HorizontalAlignment','right','Vert','middle',...
                'EdgeColor','none','Margin',2,'FontWeight','bold');
        end

    end
end


end